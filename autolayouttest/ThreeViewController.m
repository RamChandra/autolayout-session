
//
//  ThreeViewController.m
//  autolayouttest
//
//  Created by Darshan Sonde on 12/10/14.
//  Copyright (c) 2014 Darshan Sonde. All rights reserved.
//

#import "ThreeViewController.h"

// COMPRESSION RESISTANCE

@interface ThreeViewController ()
@property (nonatomic, strong) UIButton *button1;
@property (nonatomic, strong) UIButton *button2;

@end

@implementation ThreeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];//create the button and add as subview
    [self setupConstraints];
}


-(void) setupConstraints {
    NSDictionary *varDict  = NSDictionaryOfVariableBindings(_button1,_button2);
    NSArray *consArray = [NSLayoutConstraint constraintsWithVisualFormat:@"|-0-[_button1]-0-[_button2]"
                                                                 options:0 metrics:nil
                                                                   views:varDict];
    [self.view addConstraints:consArray];

    //step 1, make button 2 not go beyond screen
    //step 2, make first button ellipsis

    NSLayoutConstraint *y1 = [NSLayoutConstraint constraintWithItem:self.button1 attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self.view attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.0 constant:0.0];
    [self.view addConstraint:y1];

    NSLayoutConstraint *y2 = [NSLayoutConstraint constraintWithItem:self.button2 attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self.view attribute:NSLayoutAttributeCenterY
                                                        multiplier:1.0 constant:0.0];
    [self.view addConstraint:y2];

}





















/*
 step 1
 NSArray *border = [NSLayoutConstraint constraintsWithVisualFormat:@"[_button2]-(>=0)-|"
 options:0 metrics:nil
 views:varDict];
 [self.view addConstraints:border];
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 step 2
 [self.button1 setContentCompressionResistancePriority:249 forAxis:UILayoutConstraintAxisHorizontal];

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 */





-(void) button1Action:(UIButton*)sender {
    [self.button1 setTitle:@"Hello Autolayout World!" forState:UIControlStateNormal];
}
-(void) button2Action:(UIButton*)sender {
    [self.button2 setTitle:@"Hello Autolayout World!" forState:UIControlStateNormal];
}

















-(void) loadView {
    self.view = ({
        UIView *v = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        v.backgroundColor = [UIColor whiteColor];
        v.translatesAutoresizingMaskIntoConstraints = NO;
        v;
    });
}

-(void) setupUI {
    self.button1 = ({
        UIButton *b = [UIButton buttonWithType:UIButtonTypeCustom];
        [b setTitle:@"Hello" forState:UIControlStateNormal];
        b.translatesAutoresizingMaskIntoConstraints = NO;
        b.backgroundColor = [UIColor blueColor];
        [b setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [b addTarget:self action:@selector(button1Action:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:b];
        b;
    });
    
    self.button2 = ({
        UIButton *b = [UIButton buttonWithType:UIButtonTypeCustom];
        [b setTitle:@"Hello" forState:UIControlStateNormal];
        b.translatesAutoresizingMaskIntoConstraints = NO;
        b.backgroundColor = [UIColor magentaColor];
        [b setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [b addTarget:self action:@selector(button2Action:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:b];
        b;
    });
}

@end
