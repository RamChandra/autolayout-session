//
//  EightViewController.m
//  autolayouttest
//
//  Created by Darshan Sonde on 10/14/14.
//  Copyright (c) 2014 Darshan Sonde. All rights reserved.
//

#import "EightViewController.h"

// XIB NSIBPrototypingLayoutConstraint

@interface EightViewController ()
@property (nonatomic, strong) IBOutlet UIButton *button1;
@property (nonatomic, strong) IBOutlet UIButton *button2;
@property (nonatomic, strong) IBOutlet UILabel  *label1;

@property (nonatomic, strong) IBOutlet UIView *spacer1;
@property (nonatomic, strong) IBOutlet UIView *spacer2;
@property (nonatomic, strong) IBOutlet UIView *spacer3;

@end

@implementation EightViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupConstraints];
}


-(void) setupConstraints {
    NSDictionary *varDict  = NSDictionaryOfVariableBindings(_button1,_button2);
    NSArray *consArray = [NSLayoutConstraint constraintsWithVisualFormat:@"|-0-[_button1]-0-[_button2]"
                                                                 options:0 metrics:nil
                                                                   views:varDict];
    [self.view addConstraints:consArray];
    
    NSArray *border = [NSLayoutConstraint constraintsWithVisualFormat:@"[_button2]-(>=0)-|"
                                                              options:0 metrics:nil
                                                                views:varDict];
    [self.view addConstraints:border];
    
    //equal buttons
    NSLayoutConstraint *eq = [NSLayoutConstraint constraintWithItem:self.button1 attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.button2 attribute:NSLayoutAttributeWidth
                                                         multiplier:1.0 constant:0.0];
    [self.view addConstraint:eq];
    
    NSLayoutConstraint *l2 = [NSLayoutConstraint constraintWithItem:self.label1 attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self.view attribute:NSLayoutAttributeCenterX
                                                         multiplier:1.0 constant:0.0];
    [self.view addConstraint:l2];
    
    self.label1.preferredMaxLayoutWidth = 200.0;//multiline text
    
    
    
    //step1, y and height for spacers
    NSDictionary *yDict = NSDictionaryOfVariableBindings(_button1,_button2,_label1,_spacer1,_spacer2,_spacer3);
    
    NSArray *yConstraints = [NSLayoutConstraint constraintsWithVisualFormat:
                             @"V:|-0-[_spacer1]-0-[_label1]-0-[_spacer2(==_spacer1)]-0-[_button1]-0-[_spacer3(==_spacer1)]-0-|"
                                                                    options:0 metrics:nil
                                                                      views:yDict];
    [self.view addConstraints:yConstraints];
    
    NSArray *yConstraints2 = [NSLayoutConstraint constraintsWithVisualFormat:
                              @"V:[_spacer2]-0-[_button2]"
                                                                     options:0 metrics:nil
                                                                       views:yDict];
    [self.view addConstraints:yConstraints2];
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //x for spacers
    NSLayoutConstraint *spacer1x = [NSLayoutConstraint constraintWithItem:self.spacer1 attribute:NSLayoutAttributeCenterX
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self.view attribute:NSLayoutAttributeCenterX
                                                               multiplier:1.0 constant:0.0];
    [self.view addConstraint:spacer1x];
    NSLayoutConstraint *spacer2x = [NSLayoutConstraint constraintWithItem:self.spacer2 attribute:NSLayoutAttributeLeft
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self.spacer1 attribute:NSLayoutAttributeLeft
                                                               multiplier:1.0 constant:0.0];
    [self.view addConstraint:spacer2x];
    NSLayoutConstraint *spacer3x = [NSLayoutConstraint constraintWithItem:self.spacer3 attribute:NSLayoutAttributeLeft
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self.spacer1 attribute:NSLayoutAttributeLeft
                                                               multiplier:1.0 constant:0.0];
    [self.view addConstraint:spacer3x];
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //width for spacers
    NSLayoutConstraint *spacer1w = [NSLayoutConstraint constraintWithItem:self.spacer1 attribute:NSLayoutAttributeWidth
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:nil attribute:NSLayoutAttributeNotAnAttribute
                                                               multiplier:0.0 constant:100.0];
    [self.view addConstraint:spacer1w];
    NSLayoutConstraint *spacer2w = [NSLayoutConstraint constraintWithItem:self.spacer2 attribute:NSLayoutAttributeWidth
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self.spacer1 attribute:NSLayoutAttributeWidth
                                                               multiplier:0.0 constant:100.0];
    [self.view addConstraint:spacer2w];
    NSLayoutConstraint *spacer3w = [NSLayoutConstraint constraintWithItem:self.spacer3 attribute:NSLayoutAttributeWidth
                                                                relatedBy:NSLayoutRelationEqual
                                                                   toItem:self.spacer1 attribute:NSLayoutAttributeWidth
                                                               multiplier:0.0 constant:100.0];
    [self.view addConstraint:spacer3w];
    
    
    
}


































-(IBAction) button1Action:(UIButton*)sender {
    [self.button1 setTitle:@"Hello Eq World!" forState:UIControlStateNormal];
    self.label1.text = @"Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World Hello Eq World!";
}
-(IBAction) button2Action:(UIButton*)sender {
    [self.button2 setTitle:@"Hello Autolayout World!" forState:UIControlStateNormal];
    self.label1.text = @"Hello Autolayout WorldHello Autolayout WorldHello Autolayout WorldHello Autolayout WorldHello Autolayout!";
}



















@end
